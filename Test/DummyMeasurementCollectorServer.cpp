//
// Created by mchodzikiewicz on 4/7/19.
//

#include "DummyMeasurementCollectorServer.h"

DummyMeasurementCollectorServer::DummyMeasurementCollectorServer(Communication::DummyHttpServer & server) : Server(server) {
    //TODO: make dummy sensor deviceId sensible
    Server.addPostResource("/api/uc/calibration/123"
            ,[this](etl::istring & buff,etl::string_view content) -> Communication::HttpResponse {
                registeredCalibrationMeasurements.emplace_back(std::string(content.cbegin(), content.cend()));
                if(CalibrationCurrentResponseQueue.size()){
                    buff.assign(CalibrationCurrentResponseQueue.front().c_str());
                    CalibrationCurrentResponseQueue.pop();
                } else {
                    buff.assign(CalibrationCurrentResponse.c_str());
                }
                return {200,buff};
            });
    Server.addPostResource("/api/uc/measurements/123"
        ,[this](etl::istring & buff,etl::string_view content) -> Communication::HttpResponse {
            registeredMultiMeasurements.emplace_back(std::string(content.cbegin(), content.cend()));
            //TODO: this is a proof of not testings multimeas res
            if(MultiCurrentResponseQueue.size()){
                buff.assign(MultiCurrentResponseQueue.front().c_str());
                MultiCurrentResponseQueue.pop();
            } else {
                buff.assign(MultiCurrentResponse.c_str());
            }
            return {200,buff};
        });
    Server.addPostResource("/api/ud/devices/register/123"
        ,[this](etl::istring & buff,etl::string_view content) -> Communication::HttpResponse {
            registeredRegisters.emplace_back(std::string(content.cbegin(), content.cend()));
            if(RegisterCurrentResponseQueue.size()){
                buff.assign(RegisterCurrentResponseQueue.front().c_str());
                RegisterCurrentResponseQueue.pop();
            } else {
                buff.assign(RegisterCurrentResponse.c_str());
            }
            return {200,buff};
        });
}

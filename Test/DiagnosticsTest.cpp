//
// Created by edwin on 21/03/19.
//

#include "gtest/gtest.h"
#include <System.h>
#include <Accelerometer.h>
#include <Thermometer.h>
#include <HumiditySensor.h>
#include <DummyHttpServer.h>

#include <SensorSimulator.h>
#include <DummySystem.h>
#include <Diagnostics.h>

using DummyAccelerometer = Sensor::SensorSimulator<Sensor::Accelerometer>;
using DummyHumiditySensor = Sensor::SensorSimulator<Sensor::HumiditySensor>;
using DummyThermometer = Sensor::SensorSimulator<Sensor::Thermometer>;


class DiagnosticsTest : public ::testing::Test {
public:
    void SetUp() override {
        sys = new System::DummySystem();
        accelerometer = new DummyAccelerometer();
        thermometer = new DummyThermometer();
        humiditySensor = new DummyHumiditySensor();
        dummyHttpServer = new Communication::DummyHttpServer();
    };

    void TearDown() override {
        delete sys;
        delete accelerometer;
        delete thermometer;
        delete humiditySensor;
        delete dummyHttpServer;
    };
    System::DummySystem *sys;
    DummyAccelerometer *accelerometer;
    DummyThermometer *thermometer;
    DummyHumiditySensor *humiditySensor;
    Communication::DummyHttpServer *dummyHttpServer;


};


TEST_F(DiagnosticsTest, Init) {
    Diagnostics diagnostics(*sys, *accelerometer, *thermometer, *humiditySensor, *dummyHttpServer);

    diagnostics.start();
    etl::string<256> resBuf;
    for(int i=0;i<10;i++) {
        accelerometer->addMeasuement(Sensor::Accelerometer::measurement_type{0, 0, 1000});
    }
    Communication::HttpResponse response = dummyHttpServer->get(resBuf, "", 0, "/diagnostics");
    EXPECT_STREQ(
            "Device ID: <br>X: 1000<br>Y: 0<br>Z:0<br>Temperature: 123.000000<br>"
            "Humidity: 123.000000<br>Battery: 4.000000<br>Pitch: 90.00000 | Roll: 0.00000<br>",
            response.Response.value().cbegin());
}


TEST_F(DiagnosticsTest, ChangingValues) {
    Diagnostics diagnostics(*sys, *accelerometer, *thermometer, *humiditySensor, *dummyHttpServer);

    sys->setDeviceId("666");
    for(int i=0;i<10;i++) {
        accelerometer->addMeasuement(Sensor::Accelerometer::measurement_type{0, 300, 1000});
    }
    thermometer->addMeasuement(18);
    humiditySensor->addMeasuement(23);

    diagnostics.start();
    etl::string<256> resBuf;
    Communication::HttpResponse response = dummyHttpServer->get(resBuf, "", 0, "/diagnostics");
    EXPECT_STREQ(
            "Device ID: 666<br>X: 1000<br>Y: 0<br>Z:300<br>Temperature: 18.000000<br>Humidity: 23.000000"
            "<br>Battery: 4.000000<br>Pitch: 73.30075 | Roll: 0.00000<br>",
            response.Response.value().cbegin());
}
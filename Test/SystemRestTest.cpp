//
// Created by mchodzikiewicz on 5/12/19.
//

#include "gtest/gtest.h"
#include <System.h>
#include <DummyHttpServer.h>
#include <DummySystem.h>
#include <DummyRegistry.h>
#include "SystemRest.h"


class SystemRestTest : public ::testing::Test {
public:
    void SetUp() override {
        sys = new System::DummySystem();
        registry = new System::DummyRegistry();
        dummyHttpServer = new Communication::DummyHttpServer();
    };
    void TearDown() override {
        delete sys;
        delete registry;
        delete dummyHttpServer;
    };
    System::DummySystem * sys;
    System::DummyRegistry * registry;

    Communication::DummyHttpServer * dummyHttpServer;


};


TEST_F(SystemRestTest, Erase){
    SystemRest systemRest(*sys,*registry,*dummyHttpServer);

    systemRest.start();
    etl::string<100> resBuf;
    etl::string<100> reqBuf;
    registry->storeEntry("Test",(int)1);
    EXPECT_EQ(registry->getEntry("Test").value().get<int>(),1);
    Communication::HttpResponse response = dummyHttpServer->post(reqBuf,resBuf,"",0,"/erase","");
    EXPECT_STREQ("Erased",response.Response.value().cbegin());
    EXPECT_EQ(registry->getEntry("Test"),std::nullopt);
}


TEST_F(SystemRestTest, Reboot){
    SystemRest systemRest(*sys,*registry,*dummyHttpServer);

    systemRest.start();
    etl::string<100> resBuf;
    etl::string<100> reqBuf;
    EXPECT_EQ(sys->rebootCounter,0);
    Communication::HttpResponse response = dummyHttpServer->post(reqBuf,resBuf,"",0,"/reboot","");
    EXPECT_STREQ("Reboot",response.Response.value().cbegin());
    EXPECT_EQ(sys->rebootCounter,1);
}

TEST_F(SystemRestTest, Sleep){
    SystemRest systemRest(*sys,*registry,*dummyHttpServer);

    systemRest.start();
    etl::string<100> resBuf;
    etl::string<100> reqBuf;
    EXPECT_EQ(sys->deepSleepCounter,0);
    EXPECT_THROW(dummyHttpServer->post(reqBuf,resBuf,"",0,"/sleep",""),std::nullopt_t);
    //TODO: SystemRest should return acknowledge and go to sleep after delay
//    EXPECT_STREQ("Sleep",response.Response.value().cbegin());
    EXPECT_EQ(sys->deepSleepCounter,1);
}

TEST_F(SystemRestTest, SleepForMs){
    using namespace std::chrono_literals;
    SystemRest systemRest(*sys,*registry,*dummyHttpServer);

    systemRest.start();
    etl::string<100> resBuf;
    etl::string<100> reqBuf;
    EXPECT_EQ(sys->deepSleepCounter,0);
//    Communication::HttpResponse response = dummyHttpServer->post(resBuf,"",0,"/sleepForMs","100");
    EXPECT_THROW(dummyHttpServer->post(reqBuf,resBuf,"",0,"/sleepForMs","100"),std::chrono::milliseconds);
    //TODO: SystemRest should return acknowledge and go to sleep after delay
//    EXPECT_STREQ("Sleep for 100ms",response.Response.value().cbegin());
    EXPECT_EQ(sys->deepSleepCounter,1);
    EXPECT_EQ(sys->sleepTime,100ms);


    //TODO: SystemRest should return acknowledge and go to sleep after delay
//    response = dummyHttpServer->post(resBuf,"",0,"/sleepForMs","12345");
    EXPECT_THROW(dummyHttpServer->post(reqBuf,resBuf,"",0,"/sleepForMs","12345"),std::chrono::milliseconds);
//    EXPECT_STREQ("Sleep for 12345ms",response.Response.value().cbegin());
    EXPECT_EQ(sys->deepSleepCounter,2);
    EXPECT_EQ(sys->sleepTime,12345ms);
}

//
// Created by mchodzikiewicz on 3/27/19.
//

#include <deque>
#include <thread>
#include "gtest/gtest.h"
#include "DummyMeasurementCollectorServer.h"
#include "MeasurementCollector.h"

class MeasuremntCollectorTest : public ::testing::Test {
public:
    void SetUp() override {
        server = new Communication::DummyHttpServer;
        mc_server = new DummyMeasurementCollectorServer(*server);
        collector = new MeasurementCollector(*server);
        reqBuffer = new etl::string<1024>;
        resBuffer = new etl::string<1024>;
    };
    void TearDown() override {
        delete server;
        delete mc_server;
        delete collector;
        delete reqBuffer;
        delete resBuffer;
    };

    Communication::DummyHttpServer * server;
    DummyMeasurementCollectorServer * mc_server;
    MeasurementCollector * collector;
    etl::string<1024> * reqBuffer;
    etl::string<1024> * resBuffer;

};


TEST_F(MeasuremntCollectorTest, Init){
    EXPECT_STREQ("",mc_server->CalibrationCurrentResponse.c_str());
    EXPECT_EQ(0,mc_server->registeredCalibrationMeasurements.size());
    EXPECT_EQ(0,mc_server->registeredMultiMeasurements.size());
}

TEST_F(MeasuremntCollectorTest, SendCalibrationDefault){
    Measurement meas;

    auto res = collector->sendCalibration(*reqBuffer,*resBuffer,"123", SystemStateInfo(), meas);
    EXPECT_EQ(1,mc_server->registeredCalibrationMeasurements.size());
    EXPECT_EQ(0,mc_server->registeredMultiMeasurements.size());
    EXPECT_TRUE(!res.has_value());

}

TEST_F(MeasuremntCollectorTest, SendCalibrationResNormal300){
    using namespace std::literals;
    Measurement meas = {
        0.5,
        18.0,
        std::chrono::system_clock::from_time_t(1)
    };
    SystemStateInfo info = {
        5s,
        4.1,
        50,
        std::chrono::system_clock::from_time_t(1),
        "1.0"
    };
    mc_server->CalibrationCurrentResponse = R"(["NORMAL","300","5"])";
    auto res = collector->sendCalibration(*reqBuffer,*resBuffer,"123", info, meas);
    EXPECT_EQ(1,mc_server->registeredCalibrationMeasurements.size());
    etl::string_view expected_res = "{\"meas\":{\"ts\":\"1970-01-01T00:00:01Z\",\"temp\":18.000000,\"cosFi\":0.500000}"
                                    ",\"stat\":{\"ts\":\"1970-01-01T00:00:01Z\",\"wakeUpTime\":5000,\"batV\":4.100000,\"hum\":50.000000,\"fwVer\":\"1.0\"}"
                                    "}";
    EXPECT_STREQ(expected_res.data(),mc_server->registeredCalibrationMeasurements.at(0).c_str());
    EXPECT_EQ(0,mc_server->registeredMultiMeasurements.size());
    EXPECT_TRUE(res.value().is_type<MeasurementCollector::MeasurementSync>());
    EXPECT_EQ(res.value().get<MeasurementCollector::MeasurementSync>().wakeupInterval,300s);
    EXPECT_EQ(res.value().get<MeasurementCollector::MeasurementSync>().measurementsPerPacket,5);
}

TEST_F(MeasuremntCollectorTest, SendCalibrationResNormal15){
    using namespace std::literals;
    Measurement meas = {
        0.5,
        18.0,
        std::chrono::system_clock::from_time_t(1)
    };
    SystemStateInfo info = {
        5s,
        4.1,
        50,
        std::chrono::system_clock::from_time_t(1),
        "1.0"
    };
    mc_server->CalibrationCurrentResponse = R"(["NORMAL","15","300"])";
    auto res = collector->sendCalibration(*reqBuffer,*resBuffer,"123", info, meas);
    EXPECT_EQ(1,mc_server->registeredCalibrationMeasurements.size());
    etl::string_view expected_res = "{\"meas\":{\"ts\":\"1970-01-01T00:00:01Z\",\"temp\":18.000000,\"cosFi\":0.500000}"
                                    ",\"stat\":{\"ts\":\"1970-01-01T00:00:01Z\",\"wakeUpTime\":5000,\"batV\":4.100000,\"hum\":50.000000,\"fwVer\":\"1.0\"}}";
    EXPECT_STREQ(expected_res.data(),mc_server->registeredCalibrationMeasurements.at(0).c_str());
    EXPECT_EQ(0,mc_server->registeredMultiMeasurements.size());
    EXPECT_TRUE(res.value().is_type<MeasurementCollector::MeasurementSync>());
    EXPECT_EQ(res.value().get<MeasurementCollector::MeasurementSync>().wakeupInterval,15s);
    EXPECT_EQ(res.value().get<MeasurementCollector::MeasurementSync>().measurementsPerPacket,300);
}

TEST_F(MeasuremntCollectorTest, SendCalibrationWifiAP){
    using namespace std::literals;
    Measurement meas = {
        1.5,
        150.0,
        std::chrono::system_clock::from_time_t(800000000)
    };
    SystemStateInfo info = {
        1234ms,
        1.3456,
        12,
        std::chrono::system_clock::from_time_t(1000000000),
        "1.0"
    };
    mc_server->CalibrationCurrentResponse = "[\"WIFIAP\"]";
    auto res = collector->sendCalibration(*reqBuffer,*resBuffer,"123", info, meas);
    EXPECT_EQ(1,mc_server->registeredCalibrationMeasurements.size());
    etl::string_view expected_res = "{\"meas\":{\"ts\":\"1995-05-09T06:13:20Z\",\"temp\":150.000000,\"cosFi\":1.500000}"
                                    ",\"stat\":{\"ts\":\"2001-09-09T01:46:40Z\",\"wakeUpTime\":1234,\"batV\":1.345600,\"hum\":12.000000,\"fwVer\":\"1.0\"}}";
    EXPECT_STREQ(expected_res.data(),mc_server->registeredCalibrationMeasurements.at(0).c_str());
    EXPECT_EQ(0,mc_server->registeredMultiMeasurements.size());
    EXPECT_TRUE(res.value().is_type<MeasurementCollector::WifiAP>());
}

TEST_F(MeasuremntCollectorTest, SendCalibrationCalibration){
    using namespace std::literals;
    Measurement meas = {
        1.5,
        150.0,
        std::chrono::system_clock::from_time_t(800000000)
    };
    SystemStateInfo info = {
        1234ms,
        1.3456,
        12,
        std::chrono::system_clock::from_time_t(1000000000),
        "1.0"
    };
    mc_server->CalibrationCurrentResponse = "[\"CALIBRATION\"]";
    auto res = collector->sendCalibration(*reqBuffer,*resBuffer,"123", info, meas);
    EXPECT_EQ(1,mc_server->registeredCalibrationMeasurements.size());
    etl::string_view expected_res = "{\"meas\":{\"ts\":\"1995-05-09T06:13:20Z\",\"temp\":150.000000,\"cosFi\":1.500000}"
                                    ",\"stat\":{\"ts\":\"2001-09-09T01:46:40Z\",\"wakeUpTime\":1234,\"batV\":1.345600,\"hum\":12.000000,\"fwVer\":\"1.0\"}}";
    EXPECT_STREQ(expected_res.data(),mc_server->registeredCalibrationMeasurements.at(0).c_str());
    EXPECT_EQ(0,mc_server->registeredMultiMeasurements.size());
    EXPECT_TRUE(res.value().is_type<MeasurementCollector::Calibration>());
}

TEST_F(MeasuremntCollectorTest, SendCalibrationRefWater){
    using namespace std::literals;
    Measurement meas = {
            1.5,
            150.0,
            std::chrono::system_clock::from_time_t(800000000)
    };
    SystemStateInfo info = {
            1234ms,
            1.3456,
            12,
            std::chrono::system_clock::from_time_t(1000000000),
            "1.0"
    };
    mc_server->CalibrationCurrentResponse = "[\"WATER_REF_POINT\"]";
    auto res = collector->sendCalibration(*reqBuffer,*resBuffer,"123", info, meas);
    EXPECT_EQ(1,mc_server->registeredCalibrationMeasurements.size());
    etl::string_view expected_res = "{\"meas\":{\"ts\":\"1995-05-09T06:13:20Z\",\"temp\":150.000000,\"cosFi\":1.500000}"
                                    ",\"stat\":{\"ts\":\"2001-09-09T01:46:40Z\",\"wakeUpTime\":1234,\"batV\":1.345600,\"hum\":12.000000,\"fwVer\":\"1.0\"}}";
    EXPECT_STREQ(expected_res.data(),mc_server->registeredCalibrationMeasurements.at(0).c_str());
    EXPECT_EQ(0,mc_server->registeredMultiMeasurements.size());
    EXPECT_TRUE(res.value().is_type<MeasurementCollector::WaterRefPoint>());
}

TEST_F(MeasuremntCollectorTest, SendCalibrationRefWort){
    using namespace std::literals;
    Measurement meas = {
            1.5,
            150.0,
            std::chrono::system_clock::from_time_t(800000000)
    };
    SystemStateInfo info = {
            1234ms,
            1.3456,
            12,
            std::chrono::system_clock::from_time_t(1000000000),
            "1.0"
    };
    mc_server->CalibrationCurrentResponse = R"(["WORT_REF_POINT","12.43"])";
    auto res = collector->sendCalibration(*reqBuffer,*resBuffer,"123", info, meas);
    EXPECT_EQ(1,mc_server->registeredCalibrationMeasurements.size());
    etl::string_view expected_res = "{\"meas\":{\"ts\":\"1995-05-09T06:13:20Z\",\"temp\":150.000000,\"cosFi\":1.500000}"
                                    ",\"stat\":{\"ts\":\"2001-09-09T01:46:40Z\",\"wakeUpTime\":1234,\"batV\":1.345600,\"hum\":12.000000,\"fwVer\":\"1.0\"}}";
    EXPECT_STREQ(expected_res.data(),mc_server->registeredCalibrationMeasurements.at(0).c_str());
    EXPECT_EQ(0,mc_server->registeredMultiMeasurements.size());
    EXPECT_TRUE(res.value().is_type<MeasurementCollector::WortRefPoint>());
    EXPECT_FLOAT_EQ(res.value().get<MeasurementCollector::WortRefPoint>().WortBlg,12.43);
}

TEST_F(MeasuremntCollectorTest, SendCalibrationFOTA){
    using namespace std::literals;
    Measurement meas = {
        1.5,
        150.0,
        std::chrono::system_clock::from_time_t(800000000)
    };
    SystemStateInfo info = {
        1234ms,
        1.3456,
        12,
        std::chrono::system_clock::from_time_t(1000000000),
        "1.0"
    };
    mc_server->CalibrationCurrentResponse = "[\"FOTA\",\"magicUrl\"]";
    auto res = collector->sendCalibration(*reqBuffer,*resBuffer,"123", info, meas);
    EXPECT_EQ(1,mc_server->registeredCalibrationMeasurements.size());
    etl::string_view expected_res = "{\"meas\":{\"ts\":\"1995-05-09T06:13:20Z\",\"temp\":150.000000,\"cosFi\":1.500000}"
                                    ",\"stat\":{\"ts\":\"2001-09-09T01:46:40Z\",\"wakeUpTime\":1234,\"batV\":1.345600,\"hum\":12.000000,\"fwVer\":\"1.0\"}}";
    EXPECT_STREQ(expected_res.data(),mc_server->registeredCalibrationMeasurements.at(0).c_str());
    EXPECT_EQ(0,mc_server->registeredMultiMeasurements.size());
    EXPECT_TRUE(res.value().is_type<MeasurementCollector::Fota>());
    EXPECT_STREQ("magicUrl",res.value().get<MeasurementCollector::Fota>().url.c_str());
}

TEST_F(MeasuremntCollectorTest, SendCalibrationRefurbish){
    using namespace std::literals;
    Measurement meas = {
        1.5,
        150.0,
        std::chrono::system_clock::from_time_t(800000000)
    };
    SystemStateInfo info = {
        1234ms,
        1.3456,
        12,
        std::chrono::system_clock::from_time_t(1000000000),
        "1.0"
    };
    mc_server->CalibrationCurrentResponse = "[\"RESET\"]";
    auto res = collector->sendCalibration(*reqBuffer,*resBuffer,"123", info, meas);
    EXPECT_EQ(1,mc_server->registeredCalibrationMeasurements.size());
    etl::string_view expected_res = "{\"meas\":{\"ts\":\"1995-05-09T06:13:20Z\",\"temp\":150.000000,\"cosFi\":1.500000}"
                                    ",\"stat\":{\"ts\":\"2001-09-09T01:46:40Z\",\"wakeUpTime\":1234,\"batV\":1.345600,\"hum\":12.000000,\"fwVer\":\"1.0\"}}";
    EXPECT_STREQ(expected_res.data(),mc_server->registeredCalibrationMeasurements.at(0).c_str());
    EXPECT_EQ(0,mc_server->registeredMultiMeasurements.size());
    EXPECT_TRUE(res.value().is_type<MeasurementCollector::Refurbish>());
}

TEST_F(MeasuremntCollectorTest, SendMultipleMeasuremenst0){
    using namespace std::literals;
    std::vector<Measurement> meas;

    SystemStateInfo info{
       7s,
       8.9,
       10.11,
       std::chrono::system_clock::from_time_t(12),
       "1.0"
    };
    auto res = collector->sendMultipleMeasurement(*reqBuffer,*resBuffer,"123",info, etl::array_view<Measurement>(meas.begin(),meas.size()));
    EXPECT_EQ(0,mc_server->registeredCalibrationMeasurements.size());
    EXPECT_EQ(1,mc_server->registeredMultiMeasurements.size());
    etl::string_view expected_res = R"({"stat":{"ts":"1970-01-01T00:00:12Z","wakeUpTime":7000,"batV":8.900000,"hum":10.110000,"fwVer":"1.0"},"meas":[]})";
    EXPECT_STREQ(expected_res.data(),mc_server->registeredMultiMeasurements.at(0).c_str());
}

TEST_F(MeasuremntCollectorTest, SendMultipleMeasuremenst2){
    using namespace std::literals;
    std::vector<Measurement> meas;
    meas.push_back(Measurement{
       1,
       2,
       std::chrono::system_clock::from_time_t(3)
    });
    meas.push_back(Measurement{
       4,
       5,
       std::chrono::system_clock::from_time_t(6)
    });
    SystemStateInfo info{
       7s,
       8.9,
       10.11,
       std::chrono::system_clock::from_time_t(12),
       "1.0"
    };
    auto res = collector->sendMultipleMeasurement(*reqBuffer,*resBuffer,"123",info, etl::array_view<Measurement>(meas.begin(),meas.size()));
    EXPECT_EQ(0,mc_server->registeredCalibrationMeasurements.size());
    EXPECT_EQ(1,mc_server->registeredMultiMeasurements.size());
    etl::string_view expected_res = "{\"stat\":{\"ts\":\"1970-01-01T00:00:12Z\",\"wakeUpTime\":7000,\"batV\":8.900000,\"hum\":10.110000,\"fwVer\":\"1.0\"}"
                                    ",\"meas\":["
                                    "{\"ts\":\"1970-01-01T00:00:03Z\",\"temp\":2.000000,\"cosFi\":1.000000}"
                                    ",{\"ts\":\"1970-01-01T00:00:06Z\",\"temp\":5.000000,\"cosFi\":4.000000}"
                                    "]"
                                    "}";
    EXPECT_STREQ(expected_res.data(),mc_server->registeredMultiMeasurements.at(0).c_str());
}

TEST_F(MeasuremntCollectorTest, SendMultipleMeasuremenst4){
    using namespace std::literals;
    std::vector<Measurement> meas;
    meas.push_back(Measurement{
       1,
       2,
       std::chrono::system_clock::from_time_t(3)
    });
    meas.push_back(Measurement{
       4,
       5,
       std::chrono::system_clock::from_time_t(6)
    });
    meas.push_back(Measurement{
       4,
       5,
       std::chrono::system_clock::from_time_t(6)
    });
    meas.push_back(Measurement{
       4,
       5,
       std::chrono::system_clock::from_time_t(6)
    });
    SystemStateInfo info{
       7s,
       8.9,
       10.11,
       std::chrono::system_clock::from_time_t(12),
       "1.0"
    };
    auto res = collector->sendMultipleMeasurement(*reqBuffer,*resBuffer,"123",info, etl::array_view<Measurement>(meas.begin(),meas.size()));
    EXPECT_EQ(0,mc_server->registeredCalibrationMeasurements.size());
    EXPECT_EQ(1,mc_server->registeredMultiMeasurements.size());
    etl::string_view expected_res = "{\"stat\":{\"ts\":\"1970-01-01T00:00:12Z\",\"wakeUpTime\":7000,\"batV\":8.900000,\"hum\":10.110000,\"fwVer\":\"1.0\"}"
                                    ",\"meas\":["
                                    "{\"ts\":\"1970-01-01T00:00:03Z\",\"temp\":2.000000,\"cosFi\":1.000000}"
                                    ",{\"ts\":\"1970-01-01T00:00:06Z\",\"temp\":5.000000,\"cosFi\":4.000000}"
                                    ",{\"ts\":\"1970-01-01T00:00:06Z\",\"temp\":5.000000,\"cosFi\":4.000000}"
                                    ",{\"ts\":\"1970-01-01T00:00:06Z\",\"temp\":5.000000,\"cosFi\":4.000000}"
                                    "]"
                                    "}";
    EXPECT_STREQ(expected_res.data(),mc_server->registeredMultiMeasurements.at(0).c_str());
}

TEST_F(MeasuremntCollectorTest, RegisterDevice){
    using namespace std::literals;
    std::vector<Measurement> meas;


    mc_server->RegisterCurrentResponse.assign("HTTP/2 200 \n"
                                              "server: nginx/1.15.6\n"
                                              "date: Thu, 23 May 2019 22:19:39 GMT\n"
                                              "content-type: application/json;charset=UTF-8\n"
                                              "content-length: 43\n"
                                              "set-cookie: INGRESSCOOKIE=feae0d5edf156ed196221eabba5c337e4324b2a6; Domain=open-brewery.com; Path=/api/ud; HttpOnly\n"
                                              "cache-control: no-cache, no-store, max-age=0, must-revalidate\n"
                                              "pragma: no-cache\n"
                                              "expires: 0\n"
                                              "x-content-type-options: nosniff\n"
                                              "x-frame-options: DENY\n"
                                              "x-xss-protection: 1 ; mode=block\n"
                                              "referrer-policy: no-referrer\n"
                                              "access-control-allow-origin: *\n"
                                              "access-control-allow-credentials: true\n"
                                              "access-control-allow-methods: PUT, GET, POST, OPTIONS, DELETE\n"
                                              "access-control-allow-headers: DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Authorization\n"
                                              "strict-transport-security: max-age=15724800; includeSubDomains\n"
                                              "\n"
                                              "{\"token\":\"e7c50ffb08dd48898f4df5f16a0c784\"}");
    auto res = collector->registerDevice(*reqBuffer,*resBuffer,"123");
    EXPECT_EQ(0,mc_server->registeredCalibrationMeasurements.size());
    EXPECT_EQ(0,mc_server->registeredMultiMeasurements.size());
    EXPECT_EQ(1,mc_server->registeredRegisters.size());
    etl::string_view expected_req = "";
    etl::string_view expected_res = "e7c50ffb08dd48898f4df5f16a0c784";
    std::string Res(res.value().cbegin(),res.value().cend());
    EXPECT_STREQ(Res.c_str(),expected_res.data());
    EXPECT_STREQ(expected_req.data(),mc_server->registeredRegisters.at(0).c_str());

    mc_server->RegisterCurrentResponse.assign(R"({"token":"WOWtoken"})");
    res = collector->registerDevice(*reqBuffer,*resBuffer,"123");
    EXPECT_EQ(0,mc_server->registeredCalibrationMeasurements.size());
    EXPECT_EQ(0,mc_server->registeredMultiMeasurements.size());
    EXPECT_EQ(2,mc_server->registeredRegisters.size());
    expected_req = "";
    expected_res = "WOWtoken";
    Res.assign(res.value().cbegin(),res.value().cend());
    EXPECT_STREQ(Res.c_str(),expected_res.data());
    EXPECT_STREQ(expected_req.data(),mc_server->registeredRegisters.at(0).c_str());
}

//
// Created by mchodzikiewicz on 3/4/19.
//

#include <DummyHttpServer.h>
#include <DummyRegistry.h>
#include <WifiConfigurator.h>

#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "gmock/gmock-matchers.h"

using namespace ::testing;

class ConfigDoneMsgRouter : public etl::message_router<ConfigDoneMsgRouter,WifiConfigured,WifiConfigAborted> {
public:
    ConfigDoneMsgRouter() : message_router(0) {}
    void on_receive(etl::imessage_router& sender, const WifiConfigured& msg)  {
        wifiConfiguredCalledCnt++;
    }

    void on_receive(etl::imessage_router& sender, const WifiConfigAborted& msg)  {
        wifIConfiguredAbortedCnt++;
    }

    void on_receive_unknown(etl::imessage_router& sender, const etl::imessage& msg){
        FAIL() << "Unknown message " << msg.message_id << " received\n";
    }

    int wifiConfiguredCalledCnt = 0;
    int wifIConfiguredAbortedCnt = 0;
};

class WifiConfiguratorTest : public ::testing::Test {
public:
    void SetUp() override {
        registry = new System::DummyRegistry;
        httpServer = new Communication::DummyHttpServer();
        router = new ConfigDoneMsgRouter;
    };
    void TearDown() override {

    };

    ConfigDoneMsgRouter * router;
    System::Registry * registry;
    Communication::DummyHttpServer * httpServer;
    etl::string<128> resBuffer;
    etl::string<128> reqBuffer;

    void prestoreWifiConfig(const WifiConfigurator::WifiConfig &Config);
};

void WifiConfiguratorTest::prestoreWifiConfig(const WifiConfigurator::WifiConfig &Config) {
    registry->storeEntry("SSID", Config.SSID);
    registry->storeEntry("ApPasswd", Config.Passwd);
}


TEST_F(WifiConfiguratorTest, FreshInit){
    WifiConfigurator configurator(*registry,*httpServer,*router);
    auto config = configurator.getConfig();

    EXPECT_THAT(config, Eq(std::nullopt));
}

TEST_F(WifiConfiguratorTest, InitWithFilledRegistry){
    WifiConfigurator configurator(*registry,*httpServer,*router);
    WifiConfigurator::WifiConfig expConfig("TestSSID", "TestPasswd");
    prestoreWifiConfig(expConfig);
    configurator.init();

    auto config = configurator.getConfig().value();

    EXPECT_THAT(config.SSID, Eq(expConfig.SSID));
    EXPECT_THAT(config.Passwd, Eq(expConfig.Passwd));
}

TEST_F(WifiConfiguratorTest, SetConfigFresh){
    WifiConfigurator configurator(*registry,*httpServer,*router);
    WifiConfigurator::WifiConfig expConfig("TestSSID", "TestPasswd");
    EXPECT_TRUE(configurator.setConfig(expConfig));

    auto config = configurator.getConfig().value();

    EXPECT_THAT(config.SSID, Eq(expConfig.SSID));
    EXPECT_THAT(config.Passwd, Eq(expConfig.Passwd));
}

TEST_F(WifiConfiguratorTest, SetConfigFreshSpecialCharacters){
    WifiConfigurator configurator(*registry,*httpServer,*router);
    WifiConfigurator::WifiConfig expConfig("Te!st@#$SS^ID", "Te!st@#%Pass^wd");
    EXPECT_TRUE(configurator.setConfig(expConfig));

    auto config = configurator.getConfig().value();

    EXPECT_THAT(config.SSID, Eq(expConfig.SSID));
    EXPECT_THAT(config.Passwd, Eq(expConfig.Passwd));
}

TEST_F(WifiConfiguratorTest, ForgetConfig){
    WifiConfigurator configurator(*registry,*httpServer,*router);
    WifiConfigurator::WifiConfig expConfig("TestSSID", "TestPasswd");
    prestoreWifiConfig(expConfig);
    configurator.init();

    EXPECT_TRUE(configurator.forgetConfig());

    auto config = configurator.getConfig();
    EXPECT_THAT(config, Eq(std::nullopt));
}

TEST_F(WifiConfiguratorTest,WizardSetConfig){
    httpServer->serve("",0);
    WifiConfigurator configurator(*registry,*httpServer,*router);
    configurator.setupWizard();
    EXPECT_THAT(0, Eq(router->wifiConfiguredCalledCnt));

    WifiConfigurator::WifiConfig expConfig("TestSSID", "TestPasswd");
    etl::string<128> getMsg;

    //Fill respone to get with test credentials
    getMsg.append("ssid=").append(expConfig.SSID).append("\r\n");
    getMsg.append("passwd=").append(expConfig.Passwd).append("\r\n");

    auto response = httpServer->post(reqBuffer, resBuffer, "", 0, "/SetWifiConfig", getMsg);

    EXPECT_THAT(200, Eq(response.ResCode));
    EXPECT_THAT(response.Response.value(),
                Eq("<p>Great! You have provided WiFi credentials!<br>\n"
                   "<p>Now disconnect from the network, and BeerBob will try to connect to your home network\n"));

    EXPECT_THAT(1, Eq(router->wifiConfiguredCalledCnt));
    EXPECT_THAT(0, Eq(router->wifIConfiguredAbortedCnt));

    auto config = configurator.getConfig().value();
    EXPECT_THAT(config.SSID, Eq(expConfig.SSID));
    EXPECT_THAT(config.Passwd, Eq(expConfig.Passwd));
}

//
// Created by mchodzikiewicz on 1/17/19.
//

#include <deque>
#include <DummyHttpClient.h>
#include <DummyWifiServer.h>
#include <DummyRegistry.h>
#include <DummyFirmwareManager.h>
#include <DummySystem.h>
#include "SensorSimulator.h"
#include "gtest/gtest.h"
#include "BeerBob.h"
#include "DummyWifiClient.h"
#include "DummyMeasurementCollectorServer.h"
#include <thread>



class BeerBobTest : public ::testing::Test {
public:
    void SetUp() override {
        timer = new etl::message_timer<64>;
        registry = new System::DummyRegistry;
        fw_manager = new System::DummyFirmwareManager;
        client = new Peripheral::DummyWifiClient(*timer);
        server = new Peripheral::DummyWifiServer(*timer);
        wifi = new Peripheral::Wifi(std::move(*client),std::move(*server));
        accelerometer = new Sensor::SensorSimulator<Sensor::Accelerometer>;
        thermometer = new Sensor::SensorSimulator<Sensor::Thermometer>;
        humiditySensor = new Sensor::SensorSimulator<Sensor::HumiditySensor>;
        httpServer = new Communication::DummyHttpServer;
        extHttpServer = new Communication::DummyHttpServer;
        reqBuffer = new etl::string<2048>;
        resBuffer = new etl::string<2048>;
        sys = new System::DummySystem;
        authTokenBuffer = new etl::string<128>;
    };

    void TearDown() override {
        delete timer;
        delete registry;
        delete fw_manager;
        delete wifi;
        delete accelerometer;
        delete thermometer;
        delete humiditySensor;
        delete httpServer;
        delete extHttpServer;
        delete reqBuffer;
        delete resBuffer;
        delete sys;
        delete authTokenBuffer;
    };
    etl::message_timer<64> * timer;
    System::DummyRegistry * registry;
    System::DummyFirmwareManager * fw_manager;
    Peripheral::DummyWifiClient * client;
    Peripheral::DummyWifiServer * server;
    Peripheral::Wifi * wifi;
    Sensor::SensorSimulator<Sensor::Accelerometer> * accelerometer;
    Sensor::SensorSimulator<Sensor::Thermometer> * thermometer;
    Sensor::SensorSimulator<Sensor::HumiditySensor> * humiditySensor;
    Communication::DummyHttpServer * httpServer;
    etl::string<2048> * reqBuffer;
    etl::string<2048> * resBuffer;
    Communication::DummyHttpServer * extHttpServer;
    System::DummySystem * sys;
    etl::string<128> * authTokenBuffer;

};

TEST_F(BeerBobTest, RegisterThenCalibrate){
    using namespace std::chrono_literals;

    sys->setDeviceId("123");
    sys->Ver = "Test build";

    BeerBob bob(*sys,*registry,*fw_manager,*wifi,*accelerometer,*thermometer,*humiditySensor,*extHttpServer,*httpServer,*reqBuffer,*resBuffer,*authTokenBuffer);

    DummyMeasurementCollectorServer mc_server(*extHttpServer);

    registry->storeEntry("SSID",etl::string<32>("WifiSsid"));
    registry->storeEntry("ApPasswd",etl::string<32>("WifiPasswd"));

    for(int i=0;i<100;i++) {
        accelerometer->addMeasuement(Sensor::Accelerometer::measurement_type{0,0,1000});
    }
    for(int i=0;i<100;i++) {
        accelerometer->addMeasuement(Sensor::Accelerometer::measurement_type{0,-1000,1000});
    }
    thermometer->addMeasuement(25);
    thermometer->addMeasuement(20);
    humiditySensor->addMeasuement(40);
    humiditySensor->addMeasuement(30);

    mc_server.CalibrationCurrentResponse = "HTTP/1.1 200 OK\n"
                                     "Server: nginx/1.15.6\n"
                                     "Date: Sat, 01 Jun 2019 11:04:17 GMT\n"
                                     "Content-Type: application/json;charset=UTF-8\n"
                                     "Content-Length: 15\n"
                                     "Connection: close\n"
                                     "Cache-Control: no-cache, no-store, max-age=0, must-revalidate\n"
                                     "Pragma: no-cache\n"
                                     "Expires: 0\n"
                                     "X-Content-Type-Options: nosniff\n"
                                     "X-Frame-Options: DENY\n"
                                     "X-XSS-Protection: 1 ; mode=block\n"
                                     "Referrer-Policy: no-referrer\n"
                                     "Strict-Transport-Security: max-age=15724800; includeSubDomains\n""[\"NORMAL\",\"300\"]";
    mc_server.MultiCurrentResponse = "HTTP/1.1 200 OK\n"
                                     "Server: nginx/1.15.6\n"
                                     "Date: Sat, 01 Jun 2019 11:04:17 GMT\n"
                                     "Content-Type: application/json;charset=UTF-8\n"
                                     "Content-Length: 15\n"
                                     "Connection: close\n"
                                     "Cache-Control: no-cache, no-store, max-age=0, must-revalidate\n"
                                     "Pragma: no-cache\n"
                                     "Expires: 0\n"
                                     "X-Content-Type-Options: nosniff\n"
                                     "X-Frame-Options: DENY\n"
                                     "X-XSS-Protection: 1 ; mode=block\n"
                                     "Referrer-Policy: no-referrer\n"
                                     "Strict-Transport-Security: max-age=15724800; includeSubDomains\n"
                                     "[\"NORMAL\",\"4321\",\"12\"]";
    mc_server.RegisterCurrentResponse.assign("HTTP/2 200 \n"
                                              "server: nginx/1.15.6\n"
                                              "date: Thu, 23 May 2019 22:19:39 GMT\n"
                                              "content-type: application/json;charset=UTF-8\n"
                                              "content-length: 43\n"
                                              "set-cookie: INGRESSCOOKIE=feae0d5edf156ed196221eabba5c337e4324b2a6; Domain=open-brewery.com; Path=/api/ud; HttpOnly\n"
                                              "cache-control: no-cache, no-store, max-age=0, must-revalidate\n"
                                              "pragma: no-cache\n"
                                              "expires: 0\n"
                                              "x-content-type-options: nosniff\n"
                                              "x-frame-options: DENY\n"
                                              "x-xss-protection: 1 ; mode=block\n"
                                              "referrer-policy: no-referrer\n"
                                              "access-control-allow-origin: *\n"
                                              "access-control-allow-credentials: true\n"
                                              "access-control-allow-methods: PUT, GET, POST, OPTIONS, DELETE\n"
                                              "access-control-allow-headers: DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Authorization\n"
                                              "strict-transport-security: max-age=15724800; includeSubDomains\n"
                                              "\n"
                                              "{\"token\":\"e7c50ffb08dd48898f4df5f16a0c784\"}");

    sys->currentTimestamp = std::chrono::system_clock::from_time_t(1558281229);

    EXPECT_THROW(bob.run(false),std::chrono::milliseconds); //simulation of deep sleep

    EXPECT_STREQ(extHttpServer->Authorization.c_str(),"Authorization: Bearer e7c50ffb08dd48898f4df5f16a0c784\n");
    EXPECT_STREQ(registry->getEntry("RToken").value().get<etl::string<32>>().c_str(),"e7c50ffb08dd48898f4df5f16a0c784");
    EXPECT_EQ(mc_server.registeredRegisters.size(),1);
    EXPECT_EQ(mc_server.registeredRegisters.at(0),"");

    EXPECT_EQ(mc_server.registeredCalibrationMeasurements.size(),1);
    EXPECT_EQ(mc_server.registeredCalibrationMeasurements.at(0),"{\"meas\":{\"ts\":\"2019-05-19T15:53:49Z\",\"temp\":25.000000,\"cosFi\":90.000000},\"stat\":{\"ts\":\"2019-05-19T15:53:49Z\",\"wakeUpTime\":1000,\"batV\":4.000000,\"hum\":40.000000,\"fwVer\":\"Test build\"}}");

    EXPECT_EQ(mc_server.registeredMultiMeasurements.size(),1);

    EXPECT_EQ(mc_server.registeredMultiMeasurements.at(0),  "{\"stat\":{\"ts\":\"2019-05-19T15:53:49Z\",\"wakeUpTime\":1000,\"batV\":4.000000,\"hum\":30.000000,\"fwVer\":\"Test build\"},\"meas\":[{\"ts\":\"2019-05-19T15:53:49Z\",\"temp\":20.000000,\"cosFi\":45.000000}]}");
    EXPECT_EQ(registry->getEntry("WkupInterval").value().get<std::chrono::milliseconds>().count(),4321000);
    EXPECT_EQ(registry->getEntry("MeasPPackets").value().get<int>(),12);
}

TEST_F(BeerBobTest, Measure){
    using namespace std::chrono_literals;
    BeerBob bob(*sys,*registry,*fw_manager,*wifi,*accelerometer,*thermometer,*humiditySensor,*extHttpServer,*httpServer,*reqBuffer,*resBuffer,*authTokenBuffer);

    sys->setDeviceId("123");

    DummyMeasurementCollectorServer mc_server(*extHttpServer);

    registry->storeEntry("SSID",etl::string<32>("WifiSsid"));
    registry->storeEntry("ApPasswd",etl::string<32>("WifiPasswd"));
    registry->storeEntry("RToken",etl::string<32>("MagicTokenWow"));
    registry->storeEntry("CurrentState",1);
    registry->storeEntry("WkupInterval",1000ms);

    for(int i=0;i<100;i++) {
        accelerometer->addMeasuement(Sensor::Accelerometer::measurement_type{10000, 0, 0});
    }
    thermometer->addMeasuement(25);
    humiditySensor->addMeasuement(40);

    mc_server.MultiCurrentResponse = "HTTP/1.1 200 OK\n"
                                     "Server: nginx/1.15.6\n"
                                     "Date: Sat, 01 Jun 2019 11:04:17 GMT\n"
                                     "Content-Type: application/json;charset=UTF-8\n"
                                     "Content-Length: 15\n"
                                     "Connection: close\n"
                                     "Cache-Control: no-cache, no-store, max-age=0, must-revalidate\n"
                                     "Pragma: no-cache\n"
                                     "Expires: 0\n"
                                     "X-Content-Type-Options: nosniff\n"
                                     "X-Frame-Options: DENY\n"
                                     "X-XSS-Protection: 1 ; mode=block\n"
                                     "Referrer-Policy: no-referrer\n"
                                     "Strict-Transport-Security: max-age=15724800; includeSubDomains\n"
                                     "\n"
                                     "[\"NORMAL\",\"123\",\"12345\"]";

    sys->currentTimestamp = std::chrono::system_clock::from_time_t(1558281229);

    EXPECT_THROW(bob.run(false),std::chrono::milliseconds); //simulation of deep sleep

    EXPECT_STREQ(extHttpServer->Authorization.c_str(),"Authorization: Bearer MagicTokenWow\n");

    EXPECT_EQ(mc_server.registeredRegisters.size(),0);
    EXPECT_EQ(mc_server.registeredCalibrationMeasurements.size(),0);

    EXPECT_EQ(mc_server.registeredMultiMeasurements.size(),1);
    EXPECT_EQ(mc_server.registeredMultiMeasurements.at(0)
            ,"{\"stat\":{\"ts\":\"2019-05-19T15:53:49Z\",\"wakeUpTime\":1000,\"batV\":4.000000,\"hum\":40.000000,\"fwVer\":\"\"},\"meas\":[{\"ts\":\"2019-05-19T15:53:49Z\",\"temp\":25.000000,\"cosFi\":0.000000}]}"
    );
    EXPECT_EQ(registry->getEntry("WkupInterval").value().get<std::chrono::milliseconds>().count(),123000);
    EXPECT_EQ(registry->getEntry("MeasPPackets").value().get<int>(),12345);
}


TEST_F(BeerBobTest, FotaDuringMeasurement){
    using namespace std::chrono_literals;
    BeerBob bob(*sys,*registry,*fw_manager,*wifi,*accelerometer,*thermometer,*humiditySensor,*extHttpServer,*httpServer,*reqBuffer,*resBuffer,*authTokenBuffer);

    sys->setDeviceId("123");

    DummyMeasurementCollectorServer mc_server(*extHttpServer);

    registry->storeEntry("SSID",etl::string<32>("WifiSsid"));
    registry->storeEntry("ApPasswd",etl::string<32>("WifiPasswd"));
    registry->storeEntry("RToken",etl::string<32>("MagicTokenWow"));
    registry->storeEntry("CurrentState",1);
    registry->storeEntry("WkupInterval",1000ms);

    for(int i=0; i<100; i++) {
        accelerometer->addMeasuement(Sensor::Accelerometer::measurement_type{10000, 0, 0});
    }
    thermometer->addMeasuement(25);
    humiditySensor->addMeasuement(40);

    mc_server.MultiCurrentResponse = "HTTP/1.1 200 OK\n"
                                     "Server: nginx/1.15.6\n"
                                     "Date: Sat, 01 Jun 2019 11:04:17 GMT\n"
                                     "Content-Type: application/json;charset=UTF-8\n"
                                     "Content-Length: 15\n"
                                     "Connection: close\n"
                                     "Cache-Control: no-cache, no-store, max-age=0, must-revalidate\n"
                                     "Pragma: no-cache\n"
                                     "Expires: 0\n"
                                     "X-Content-Type-Options: nosniff\n"
                                     "X-Frame-Options: DENY\n"
                                     "X-XSS-Protection: 1 ; mode=block\n"
                                     "Referrer-Policy: no-referrer\n"
                                     "Strict-Transport-Security: max-age=15724800; includeSubDomains\n"
                                     "\n"
                                     "[\"FOTA\",\"magicUrl\"]";

    sys->currentTimestamp = std::chrono::system_clock::from_time_t(1558281229);

    EXPECT_THROW(bob.run(false),std::chrono::milliseconds); //simulation of deep sleep

    EXPECT_STREQ(fw_manager->Url.c_str(),"magicUrl");
}


TEST_F(BeerBobTest, FotaDuringCalibration){
    using namespace std::chrono_literals;
    BeerBob bob(*sys,*registry,*fw_manager,*wifi,*accelerometer,*thermometer,*humiditySensor,*extHttpServer,*httpServer,*reqBuffer,*resBuffer,*authTokenBuffer);

    sys->setDeviceId("123");

    DummyMeasurementCollectorServer mc_server(*extHttpServer);

    registry->storeEntry("SSID",etl::string<32>("WifiSsid"));
    registry->storeEntry("ApPasswd",etl::string<32>("WifiPasswd"));
    registry->storeEntry("RToken",etl::string<32>("MagicTokenWow"));
    registry->storeEntry("CurrentState",0);
    registry->storeEntry("WkupInterval",1000ms);

    for(int i=0; i<100; i++) {
        accelerometer->addMeasuement(Sensor::Accelerometer::measurement_type{10000, 0, 0});
    }
    thermometer->addMeasuement(25);
    humiditySensor->addMeasuement(40);

    mc_server.CalibrationCurrentResponse = "HTTP/1.1 200 OK\n"
                                     "Server: nginx/1.15.6\n"
                                     "Date: Sat, 01 Jun 2019 11:04:17 GMT\n"
                                     "Content-Type: application/json;charset=UTF-8\n"
                                     "Content-Length: 15\n"
                                     "Connection: close\n"
                                     "Cache-Control: no-cache, no-store, max-age=0, must-revalidate\n"
                                     "Pragma: no-cache\n"
                                     "Expires: 0\n"
                                     "X-Content-Type-Options: nosniff\n"
                                     "X-Frame-Options: DENY\n"
                                     "X-XSS-Protection: 1 ; mode=block\n"
                                     "Referrer-Policy: no-referrer\n"
                                     "Strict-Transport-Security: max-age=15724800; includeSubDomains\n"
                                     "\n"
                                     "[\"FOTA\",\"magicUrl\"]";

    sys->currentTimestamp = std::chrono::system_clock::from_time_t(1558281229);

    EXPECT_THROW(bob.run(false),std::chrono::milliseconds); //simulation of deep sleep

    EXPECT_STREQ(fw_manager->Url.c_str(),"magicUrl");
}

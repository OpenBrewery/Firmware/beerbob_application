//
// Created by mchodzikiewicz on 4/7/19.
//

#ifndef BEERBOB_APPLICATION_DUMMYMEASUREMENTCOLLECTORSERVER_H
#define BEERBOB_APPLICATION_DUMMYMEASUREMENTCOLLECTORSERVER_H


#include <DummyHttpServer.h>
#include "MeasurementCollector.h"
#include <queue>

class DummyMeasurementCollectorServer {
public:
    DummyMeasurementCollectorServer(Communication::DummyHttpServer & server);

    std::vector<std::string> registeredCalibrationMeasurements;
    std::vector<std::string> registeredMultiMeasurements;
    std::vector<std::string> registeredRegisters;

    std::string CalibrationCurrentResponse;
    std::string MultiCurrentResponse;
    std::string RegisterCurrentResponse;

    std::queue<std::string> CalibrationCurrentResponseQueue;
    std::queue<std::string> MultiCurrentResponseQueue;
    std::queue<std::string> RegisterCurrentResponseQueue;

    Communication::DummyHttpServer & Server;
};


#endif //BEERBOB_APPLICATION_DUMMYMEASUREMENTCOLLECTORSERVER_H

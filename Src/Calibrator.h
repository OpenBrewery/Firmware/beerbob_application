//
// Created by mchodzikiewicz on 24.09.2020.
//

#ifndef BEERBOB_APPLICATION_CALIBRATOR_H
#define BEERBOB_APPLICATION_CALIBRATOR_H


#include <Point3D.h>
#include <Registry.h>

namespace Calibration {
    class Calibrator {
    public:
        Calibrator(System::Registry &registry) : Registry(registry) {}

        std::optional<Geometry::Point3D<int>> waterReference();

        std::optional<std::pair<Geometry::Point3D<int>, float>> wortReference();

        bool setWaterReference(Geometry::Point3D<int> &ref);

        bool setWortReference(Geometry::Point3D<int> &ref, float blg);

    private:
        System::Registry &Registry;

    };
}


#endif //BEERBOB_APPLICATION_CALIBRATOR_H

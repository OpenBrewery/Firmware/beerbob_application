//
// Created by edwin on 21/03/19.
//

#ifndef BEERBOB_APPLICATION_DIAGNOSTICS_H
#define BEERBOB_APPLICATION_DIAGNOSTICS_H


#include <System.h>
#include <Accelerometer.h>
#include <Thermometer.h>
#include <HumiditySensor.h>
#include <HttpServer.h>

class Diagnostics {
public:
    Diagnostics(
            System::System & sys,
            Sensor::Accelerometer & accelerometer,
            Sensor::Thermometer & thermometer,
            Sensor::HumiditySensor & humiditySensor,
            Communication::HttpServer & httpServer
            ) : Sys(sys)
            , Accelerometer(accelerometer)
            , Thermometer(thermometer)
            , HumiditySensor(humiditySensor)
            , HttpServer(httpServer)
    {

    }

    void start();

    System::System & Sys;
    Sensor::Accelerometer & Accelerometer;
    Sensor::Thermometer & Thermometer;
    Sensor::HumiditySensor & HumiditySensor;
    Communication::HttpServer & HttpServer;

};


#endif //BEERBOB_APPLICATION_DIAGNOSTICS_H

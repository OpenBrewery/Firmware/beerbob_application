//
// Created by mchodzikiewicz on 3/4/19.
//

#include "WifiConfigurator.h"

WifiConfigurator::WifiConfigurator(System::Registry &registry
        , Communication::HttpServer & httpServer
        , etl::imessage_router & messageTarget) : Registry(registry)
                                                    , HttpServer(httpServer)
                                                    , Config(std::nullopt)
                                                    , MessageTarget(messageTarget) {}

WifiConfigurator::WifiConfig::WifiConfig(const etl::string_view &Ssid,
                                         const etl::string_view &Passwd) {
    this->SSID.assign(Ssid.begin(), Ssid.end());
    this->Passwd.assign(Passwd.begin(), Passwd.end());
}

void WifiConfigurator::init() {
    auto SsidReg = Registry.getEntry("SSID");
    auto PasswdReg = Registry.getEntry("ApPasswd");

    Config = std::nullopt;

    if(!SsidReg || !PasswdReg) {
        printf("SSID or passwd does not exist\n");
        return;
    }

    if(!SsidReg.value().is_valid() || !PasswdReg.value().is_valid()){
        printf("SSID or passwd were not valid in the registry\n");
        return;
    }

    if(!SsidReg.value().is_type<etl::string<32>>() || !PasswdReg.value().is_type<etl::string<32>>()) {
        printf("SSID or passwd is not etl::string<32> type\n");
        return;
    }

    auto Ssid = SsidReg.value().get<etl::string<32>>();
    auto Passwd = PasswdReg.value().get<etl::string<32>>();

    Ssid.repair();
    Passwd.repair();

    Config = WifiConfig(Ssid, Passwd);

    printf("Wifi Config restored\n");
}

std::optional<WifiConfigurator::WifiConfig> WifiConfigurator::getConfig() {
    return Config;
}

bool WifiConfigurator::setConfig(const WifiConfigurator::WifiConfig &config) {
    //TODO: This kind of call will hide truncating strings or raise exception inside registry
    Config = config;

    return Registry.storeEntry("SSID",config.SSID) &&
           Registry.storeEntry("ApPasswd", config.Passwd);
}

bool WifiConfigurator::forgetConfig() {
    Config = std::nullopt;

    return Registry.eraseEntry("SSID") &&
           Registry.eraseEntry("ApPasswd");
}

bool WifiConfigurator::hasConfig() {
    return Config.has_value();
}

std::optional<etl::string_view>
WifiConfigurator::getTokenValue(const etl::string_view &token,
                                const etl::string_view &separator,
                                const etl::string_view &content) const {
    auto begin = content.find(separator, content.find(token));
    auto end = content.find("\r\n", begin);

    if (begin == etl::string_view::npos || end == etl::string_view::npos) {
        return std::nullopt;
    }

    //omit separator in returned value
    begin += separator.length();

    auto len = end - begin;
    return content.substr(begin, len);
}

bool WifiConfigurator::setupWizard() {
    Config = std::nullopt;
    HttpServer.addPostResource("/SetWifiConfig",[this](etl::istring & resBuffer,etl::string_view content){
        auto Ssid = getTokenValue("ssid", "=", content);
        auto Passwd = getTokenValue("passwd", "=", content);

        if (Ssid && Passwd) {
            setConfig(WifiConfig(Ssid.value(), Passwd.value()));
        }

        MessageTarget.receive(WifiConfigured());
        return Communication::HttpResponse{200,etl::string_view(
                "<p>Great! You have provided WiFi credentials!<br>\n"
                "<p>Now disconnect from the network, and BeerBob will try to connect to your home network\n"
                )};
    });
    HttpServer.addGetResource("/",[this](etl::istring & resBuffer){
        resBuffer.resize(resBuffer.max_size());
        resBuffer.resize(snprintf(resBuffer.data(),resBuffer.size(),
        "\n"
        "<html>\n"
        "<head>\n"
        "<title>Configure your BeerBob!</title>\n"
        "\n"
        "<style>\n"
        "input[type='submit'] {\n"
        "background-color: #ff9b24;\n"
        "padding: 6px 16px;\n"
        "border-radius: 4px;\n"
        "margin: 5px;\n"
        "border: none;\n"
        "}\n"
        "input[type='submit']:hover {\n"
        "background-color: rgb(178, 108, 25);\n"
        "cursor: pointer;\n"
        "}\n"
        ".big-btn {\n"
        "font-size: 20px;\n"
        "width: 100%%;\n"
        "}\n"
        "input[type='text'],\n"
        "input[type='password'] {\n"
        "padding: 12px 20px;\n"
        "margin: 8px 0;\n"
        "}\n"
        "div {\n"
        "width: 30%%;\n"
        "margin: auto;\n"
        "display: flex;\n"
        "align-items: center;\n"
        "flex-direction: column;\n"
        "border-radius: 4px;\n"
        "box-shadow: 0px 3px 1px -2px #ccc, 0px 2px 2px 0px #dbdbdb,\n"
        "0px 1px 5px 0px #e0e0e0;\n"
        "}\n"
        "body {\n"
        "background-color: #fafafa;\n"
        "}\n"
        "p {\n"
        "font-size: 20px;\n"
        "padding-bottom: 10px;\n"
        "}\n"
        "</style>\n"
        "</head>\n"
        "\n"
        "    <body>\n"
        "        <div>\n"
        "            <p>Configure your BeerBob!</p>\n"
        "            <form enctype=\"text/plain\" accept-charset=\"UTF-8\" action=\"SetWifiConfig\" method=\"post\">\n"
        "                <input type=\"text\" placeholder=\"SSID\" name=\"ssid\" />\n"
        "                <br />\n"
        "                <input type=\"password\" placeholder=\"Password\" name=\"passwd\" />\n"
        "                <br />\n"
        "                <input type=\"submit\" class=\"big-btn\" value=\"Submit\" />\n"
        "            </form>\n"
        "        </div>\n"
        "    </body>\n"
        "</html>"
        ));
        return Communication::HttpResponse{200,etl::string_view(resBuffer)};
    });

    HttpServer.addGetResource("/favicon.ico",[this](etl::istring & resBuffer){
        resBuffer.resize(resBuffer.max_size());
        resBuffer.resize(snprintf(resBuffer.data(),resBuffer.size(),"No Icon"));
        return Communication::HttpResponse{200,etl::string_view(resBuffer)};
    });

    return false;
}

//
// Created by mchodzikiewicz on 3/27/19.
//

#include "MeasurementCollector.h"
#include <cstdio>

MeasurementCollector::MeasurementCollector(Communication::HttpClient &client) : HttpClient(client) {

}

MeasurementCollector::Settings MeasurementCollector::sendCalibration(
        etl::istring & reqBuffer
        , etl::istring & resBuffer
        , etl::string_view deviceId
        , SystemStateInfo info
        , Measurement meas
        ) {
    using namespace std::literals;

    etl::string<128> path;
    path.resize(path.max_size());
    path.resize(snprintf(path.data(),path.size(),"/api/uc/calibration/%s",deviceId.data()));

    char ts_buf_meas[30];
    char ts_buf_info[30];
    time_t ts = std::chrono::system_clock::to_time_t(meas.timestamp);
    strftime(ts_buf_meas,30,"%FT%TZ",gmtime(&ts));
    ts = std::chrono::system_clock::to_time_t(info.timestamp);
    strftime(ts_buf_info,30,"%FT%TZ",gmtime(&ts));

    etl::string<512> sendBuffer;
    sendBuffer.resize(sendBuffer.max_size());
    sendBuffer.resize(snprintf(sendBuffer.data(),sendBuffer.size(),
            R"({"meas":{"ts":"%s","temp":%f,"cosFi":%f},"stat":{"ts":"%s","wakeUpTime":%i,"batV":%f,"hum":%f,"fwVer":"%s"}})"
            ,ts_buf_meas
            ,meas.temperature
            ,meas.cosine
            ,ts_buf_info
            ,static_cast<int>(std::chrono::duration_cast<std::chrono::milliseconds>(info.lastAwakeTime).count())
            ,info.batteryVoltage
            ,info.humidity
            ,info.version.data()
            ));
    auto res = HttpClient.Spost(reqBuffer,resBuffer,"open-brewery.com",443,path,sendBuffer);
    if(res.ResCode != 200) return std::nullopt;
    if(res.Response == std::nullopt) return std::nullopt;
    return deserializeSettings(res.Response.value());
}

MeasurementCollector::Settings MeasurementCollector::sendMultipleMeasurement(
        etl::istring & reqBuffer
        , etl::istring & resBuffer
        , etl::string_view deviceId
        , SystemStateInfo info
        , etl::array_view<Measurement> meas
    ) {

    etl::string<128> path;
    path.resize(path.max_size());
    path.resize(snprintf(path.data(),path.size(),"/api/uc/measurements/%s",deviceId.data()));

    char ts_buf[30];
    time_t ts;
    ts = std::chrono::system_clock::to_time_t(info.timestamp);
    strftime(ts_buf,30,"%FT%TZ",gmtime(&ts));

    etl::string<512> sendBuffer;
    sendBuffer.resize(sendBuffer.max_size());
    size_t strSize = 0;
    strSize += snprintf(sendBuffer.data()+strSize,sendBuffer.size()-strSize
            ,R"({"stat":{"ts":"%s","wakeUpTime":%i,"batV":%f,"hum":%f,"fwVer":"%s"})"
            ,ts_buf,info.lastAwakeTime,info.batteryVoltage,info.humidity,info.version.data());

    strSize += snprintf(sendBuffer.data()+strSize,sendBuffer.size()-strSize,",\"meas\":[",deviceId.data());
    for(const Measurement & m : meas){
        if(&m != meas.begin()) {
            sendBuffer.at(strSize) = ',';
            strSize++;
        }
        time_t ts = std::chrono::system_clock::to_time_t(m.timestamp);
        strftime(ts_buf,30,"%FT%TZ",gmtime(&ts));
        strSize += snprintf(sendBuffer.data()+strSize,sendBuffer.size()-strSize
                    ,R"({"ts":"%s","temp":%f,"cosFi":%f})"
                    ,ts_buf,m.temperature,m.cosine);
    }
    strSize += snprintf(sendBuffer.data()+strSize,sendBuffer.size()-strSize,"]}");
    sendBuffer.resize(strSize);


    auto res = HttpClient.Spost(reqBuffer,resBuffer,"open-brewery.com",443,path,sendBuffer);
    if(res.ResCode != 200) return std::nullopt;
    if(res.Response == std::nullopt) return std::nullopt;
    return deserializeSettings(res.Response.value());
}


std::optional<etl::string_view> MeasurementCollector::registerDevice(etl::istring & reqBuffer, etl::istring & resBuffer, etl::string_view deviceId) {
    etl::string<128> path;
    path.resize(path.max_size());
    path.resize(snprintf(path.data(),path.size(),"/api/ud/devices/register/%s",deviceId.data()));
    auto res = HttpClient.Spost(reqBuffer,resBuffer,"open-brewery.com",443,path,"");
    if(res.ResCode != 200) {
        printf("Incorect response for register at %s\n",path.data());
        return std::nullopt;
    }
    if(res.Response == std::nullopt) {
        printf("Response had no body\n");
        return std::nullopt;
    }
    printf("Register response received\n");
    return deserializeToken(res.Response.value());
}

MeasurementCollector::Settings MeasurementCollector::deserializeSettings(etl::string_view response) {
    using namespace std::chrono_literals;
    Settings settings = std::nullopt;
    auto body_offset = response.find("NORMAL");
    if(body_offset != etl::string_view::npos) {
        etl::string_view body{&response.at(body_offset),response.size() - body_offset};
        auto wakeupInterval = atoi(body.begin() + 9);
        auto measurementsPerPacket = atoi(&body.at(body.find_last_of(',')+2));
        printf("Body offset:  %i\n"
               "Read_seconds: %i\n"
               "Read_meas_per_wifi: %i\n"
               ,body_offset
               ,wakeupInterval
               ,measurementsPerPacket
                );

        settings = MeasurementSync{
            std::chrono::seconds(wakeupInterval),
            measurementsPerPacket
        };
    } else if(response.find("CALIBRATION") != etl::string_view::npos) {
        settings = Calibration{};
    } else if(response.find("WATER_REF_POINT") != etl::string_view::npos) {
        settings = WaterRefPoint{};
    } else if(response.find("WORT_REF_POINT") != etl::string_view::npos) {
        auto body_offset = response.find("WORT_REF_POINT");
        etl::string_view body{&response.at(body_offset),response.size() - body_offset};
        char * dummy_strstop;
        auto receivedBlg = strtof(&body.at(body.find_last_of(',')+2),&dummy_strstop);
        printf("Body offset:  %i\n"
               "Read_blg: %i\n"
                ,body_offset
                ,receivedBlg
        );
        settings = WortRefPoint{static_cast<float>(receivedBlg)};
    } else if(response.find("WIFIAP") != etl::string_view::npos) {
        settings = WifiAP{};
    } else if(response.find("FOTA") != etl::string_view::npos) {
        settings = Fota{etl::string<256>{&response.at(response.find("\"FOTA\"")+8),response.cend()-2}};
    } else if(response.find("RESET") != etl::string_view::npos) {
        settings = Refurbish{};
    } else if(response.find("OFF") != etl::string_view::npos) {
        settings = Off{};
    }
    return settings;
}

std::optional<etl::string_view> MeasurementCollector::deserializeToken(etl::string_view response) {
    size_t json_pos = response.find('{');
    if(json_pos != response.npos){
        etl::string_view token;
        token.assign(&response.at(json_pos+10),response.cend()-2);
        printf("Token is %.31s\n",token.data());
        return token;
    }
    return std::nullopt;
}


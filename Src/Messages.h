//
// Created by mchodzikiewicz on 3/6/19.
//

#ifndef BEERBOB_APPLICATION_MESSAGES_H
#define BEERBOB_APPLICATION_MESSAGES_H

struct Messages {
    enum {
        WifiConfigured = 0,
        WifiConfigAborted
    };
};

#endif //BEERBOB_APPLICATION_MESSAGES_H

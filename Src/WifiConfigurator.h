//
// Created by mchodzikiewicz on 3/4/19.
//

#ifndef BEERBOB_APPLICATION_SETTINGSMANAGER_H
#define BEERBOB_APPLICATION_SETTINGSMANAGER_H


#include <Registry.h>
#include <HttpServer.h>
#include "Messages.h"
#include <etl/message_router.h>

class WifiConfigured : public etl::message<Messages::WifiConfigured> {};
class WifiConfigAborted : public etl::message<Messages::WifiConfigAborted> {};

class WifiConfigurator {
public:
    struct WifiConfig {
        etl::string<32> SSID;
        etl::string<32> Passwd;

        WifiConfig(const etl::string_view &ssid, const etl::string_view &passwd);
    };

    WifiConfigurator(System::Registry & registry, Communication::HttpServer & httpServer, etl::imessage_router & messageTarget);

    void init(); //Note that registry have to be initalized when calling init()

    bool setupWizard();
    bool setConfig(const WifiConfig & config);
    bool forgetConfig();
    std::optional<WifiConfig> getConfig();
    bool hasConfig();

private:
    std::optional<etl::string_view>
    getTokenValue(const etl::string_view &token,
                  const etl::string_view &seprator,
                  const etl::string_view &content) const;

    System::Registry & Registry;
    Communication::HttpServer & HttpServer;
    std::optional<WifiConfig> Config;
    etl::imessage_router & MessageTarget;
};

#endif //BEERBOB_APPLICATION_SETTINGSMANAGER_H

//
// Created by mchodzikiewicz on 24.09.2020.
//

#include "Calibrator.h"

std::optional<Geometry::Point3D<int>> Calibration::Calibrator::waterReference() {
    auto X = Registry.getEntry("WaterRefX");
    auto Y = Registry.getEntry("WaterRefY");
    auto Z = Registry.getEntry("WaterRefZ");
    if(X && Y && Z){
        return Geometry::Point3D<int>{X.value(),Y.value(),Z.value()};
    }
    return std::nullopt;
}

std::optional<std::pair<Geometry::Point3D<int>,float>> Calibration::Calibrator::wortReference() {
    auto X = Registry.getEntry("WortRefX");
    auto Y = Registry.getEntry("WortRefY");
    auto Z = Registry.getEntry("WortRefZ");
    auto blg = Registry.getEntry("WortRefBlg");
    if(X && Y && Z && blg){
        return std::pair{Geometry::Point3D<int>{X.value().get<int>(),Y.value().get<int>(),Z.value().get<int>()},blg.value().get<float>()};
    }
    return std::nullopt;
}

bool Calibration::Calibrator::setWaterReference(Geometry::Point3D<int> &ref) {
    return Registry.storeEntry("WaterRefX", ref.X) &&
           Registry.storeEntry("WaterRefY", ref.Y) &&
           Registry.storeEntry("WaterRefZ", ref.Z);
}

bool Calibration::Calibrator::setWortReference(Geometry::Point3D<int> &ref, float blg) {
    return Registry.storeEntry("WortRefX", ref.X) &&
           Registry.storeEntry("WortRefY", ref.Y) &&
           Registry.storeEntry("WortRefZ", ref.Z) &&
           Registry.storeEntry("WortRefBlg", blg);
}

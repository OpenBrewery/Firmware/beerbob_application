//
// Created by mchodzikiewicz on 2/11/19.
//

#include <iostream>
#include <cstdio>
#include <thread>
#include "BeerBob.h"
#include "Diagnostics.h"
#include "SystemRest.h"
#include <chrono>
#include <cstring>

using namespace std::literals;


/* Constants that aren't configurable in menuconfig */
#define WEB_SERVER "192.168.1.23"
#define WEB_PORT 7878
#define WEB_URL "/"

BeerBob::BeerBob(System::System &sys, System::Registry &registry, System::FirmwareManager &fw_manager,
                 Peripheral::Wifi &wifi, Sensor::Accelerometer &accelerometer, Sensor::Thermometer &thermometer,
                 Sensor::HumiditySensor &humiditySensor, Communication::HttpClient &httpClient,
                 Communication::HttpServer &httpServer, etl::istring &httpResBuffer, etl::istring &httpReqBuffer,
                 etl::istring &authTokenBuffer) : message_router(MessageRouters::BeerBob), Sys(sys), Registry(registry),
                                                  FwManager(fw_manager), Wifi(wifi), Accelerometer(accelerometer),
                                                  Thermometer(thermometer), HumiditySensor(humiditySensor),
                                                  HttpClient(httpClient), HttpServer(httpServer),
                                                  HttpResBuffer(httpResBuffer), HttpReqBuffer(httpReqBuffer),
                                                  AuthStringBuffer(authTokenBuffer), batteryV(0.1234) {

}

bool BeerBob::run(bool pressed) {
    using namespace std::literals;

    etl::string<20> ip_buf; //buffer for getting ip info from client

    Diagnostics diagnostics(Sys, Accelerometer, Thermometer, HumiditySensor, HttpServer);
    diagnostics.start();

    SystemRest systemRest(Sys, Registry, HttpServer);
    systemRest.start();

    MeasurementCollector collector(HttpClient);

    WifiConfigurator wifiConfigurator(Registry, HttpServer, *this);

    Calibration::Calibrator calibrator(Registry);


    printf("Starting Beerbob %s\n%s\n", Sys.getDeviceId().data(), Sys.version().data()
    );

    if (pressed) {
        printf("Button pressed, factory reset.\n");
        Registry.eraseRegistry();
        Sys.deepSleep(5s);
    } else {
        printf("Button not pressed, regular operation.\n");
    }

    batteryV = Sys.batteryVoltage();

    attemptWifiConnection(wifiConfigurator);
    if (!HttpServer.isServing()) {
        HttpServer.serve("192.168.1.1", 80);
    }

    registerIfNeeded(Registry, collector);

    while (1) {
//        storeState(MeasurementCollector::Calibration{});
        auto state = retrieveState();
        if (!state.has_value()) {
            printf("State have not been stored, setting to calibration\n");
            storeState(MeasurementCollector::Calibration());
        } else {
            if (state.value().is_type<MeasurementCollector::MeasurementSync>()) {
                measurementSynchronization(Sys, collector, calibrator);
            } else if (state.value().is_type<MeasurementCollector::Calibration>()) {
                calibrationLoop(Sys, collector, calibrator);
            } else {
                printf("Incorrect state restored, rolling back\n");
                Registry.eraseRegistry();
                break;
            }
        }
    }

    printf("Reached the end");
    Sys.deepSleep(5s);

    return true;
}

void BeerBob::measurementSynchronization(System::System &sys, MeasurementCollector &collector, Calibration::Calibrator &calibrator) {
    using namespace std::chrono_literals;
    printf("Entering measurement synchronization\n");

    auto meas = performMeasurement(calibrator);

    SystemStateInfo info;
    info.humidity = HumiditySensor.read().value();
    if (info.humidity < 0) info.humidity = 0;
    //TODO (mchodzikiewicz): this ternary is memory unsafe
    info.lastAwakeTime = sys.previousUptime() ? sys.previousUptime().value() : 0ms;
    info.timestamp = meas.timestamp;
    info.batteryVoltage = batteryV;
    info.version = sys.version();

    printf("Sending calibration point.\nCos = %f\nT = %f\nts = %i\nHum = %f\n\n", meas.cosine, meas.temperature,
           meas.timestamp, info.humidity);
    auto res = collector.sendMultipleMeasurement(HttpReqBuffer, HttpResBuffer, sys.getDeviceId(), info,
                                                 etl::array_view<Measurement>(&meas, 1));
    if (!res.has_value()) {
        printf("Incorrect measurement response\n");
    } else if (res.value().is_type<MeasurementCollector::MeasurementSync>()) {
        printf("Measurement continue response received, wakeup int: %ims, measurements per packet: %i\n",
               static_cast<int>(res.value().get<MeasurementCollector::MeasurementSync>().wakeupInterval.count()),
               res.value().get<MeasurementCollector::MeasurementSync>().measurementsPerPacket
        );
        storeState(res);
        sys.deepSleep(res.value().get<MeasurementCollector::MeasurementSync>().wakeupInterval);
    } else if (res.value().is_type<MeasurementCollector::Fota>()) {
        FwManager.downloadFirmwarePackage(res.value().get<MeasurementCollector::Fota>().url);
        //don't store FOTA state - it is a volatile state
        sys.deepSleep(1s);
    } else if (res.value().is_type<MeasurementCollector::Off>()) {
        printf("Off command - deep sleep\n");
        sys.deepSleep();
        return;
    } else if (res.value().is_type<MeasurementCollector::WaterRefPoint>()) {
        printf("Warning: Water Ref setting received during normal mode- ignoring\n");
    } else if (res.value().is_type<MeasurementCollector::WortRefPoint>()) {
        printf("Warning: Wort Ref setting received during normal mode- ignoring\n");
    } else {
        storeState(res);
        sys.deepSleep(1s);
    }
}

void BeerBob::calibrationLoop(System::System &sys, MeasurementCollector &collector, Calibration::Calibrator &calibrator) {
    using namespace std::chrono_literals;
    printf("Entering calibration loop\n");
    while (1) {
        auto meas_pair = performCalMeasurement();
        auto meas = meas_pair.first;

        SystemStateInfo info;
        info.humidity = HumiditySensor.read().value();
        if (info.humidity < 0) info.humidity = 0;
        //TODO (mchodzikiewicz): this ternary is memory unsafe
        info.lastAwakeTime = sys.previousUptime() ? sys.previousUptime().value() : 0ms;
        info.timestamp = meas.timestamp;
        info.batteryVoltage = batteryV;
        info.version = sys.version();

        printf("Sending calibration point.\nCos = %f\nT = %f\nts = %li\nHum = %f\n\n", meas.cosine, meas.temperature,
               meas.timestamp.time_since_epoch().count(), info.humidity);

        auto res = collector.sendCalibration(HttpReqBuffer, HttpResBuffer, sys.getDeviceId(), info, meas);
        if (res.has_value()) {
            //TODO (mchodzikiewicz): state handling should be performed outside, but then collector-connectable functions should return settings
            if (res.value().is_type<MeasurementCollector::Fota>()) {
                FwManager.downloadFirmwarePackage(res.value().get<MeasurementCollector::Fota>().url);
                //don't store FOTA state - it is temporary
                sys.deepSleep(1s);
            } else if (res.value().is_type<MeasurementCollector::Off>()) {
                printf("Off command - deep sleep\n");
                sys.deepSleep();
                return;
            } else if (res.value().is_type<MeasurementCollector::WaterRefPoint>()) {
                calibrator.setWaterReference(meas_pair.second);
                printf("Water Ref point - continuing calibration\n");
            } else if (res.value().is_type<MeasurementCollector::WortRefPoint>()) {
                calibrator.setWortReference(meas_pair.second, res.value().get<MeasurementCollector::WortRefPoint>().WortBlg);
                printf("Wort Ref point - continuing calibration\n");
            } else if (!res.value().is_type<MeasurementCollector::Calibration>()) {
                storeState(res);
                printf("State changed. Leaving calibration\n");
                return;
            } else {
                printf("Continuing calibration\n");
                std::this_thread::sleep_for(1s);
            }
        } else {
            printf("Incorrect state received.\n");
            sys.deepSleep(30s);
        }
        std::this_thread::sleep_for(5s);
    }
}

void BeerBob::attemptWifiConnection(WifiConfigurator &wifiConfigurator) {
    wifiConfigurator.init();
    auto wifiConfig = wifiConfigurator.getConfig();
    if (wifiConfig) {
//        wifiClient->setOnInitializedCallback([&wifiClient](){wifiClient->connect();});
//        wifiClient->setOnDisconnectedCallback([&wifiClient](){wifiClient->init();});
        printf("Connecting to Wifi with SSID: %s and Passwd: %s\n", wifiConfig.value().SSID.c_str(),
               wifiConfig.value().Passwd.c_str());
        auto wifiClient = Wifi.getClient();
        wifiClient->init();
        wifiClient->setSSID({wifiConfig.value().SSID.c_str(), wifiConfig.value().SSID.size()});
        wifiClient->setPassword({wifiConfig.value().Passwd.c_str(), wifiConfig.value().Passwd.size()});
        wifiClient->connect();
        std::chrono::milliseconds waiting_time = 0ms;
        while (!wifiClient->isConnected() && waiting_time < 20s) {
            std::this_thread::sleep_for(500ms);
            waiting_time += 500ms;
            printf("Waiting for Wifi connection\n");
        }
        if (wifiClient->isConnected()) {
            printf("Wifi Connected\n");
            Registry.storeEntry("FailWifiConn", 0);
            Registry.storeEntry("PrevSuccConn",true);;
            printf("Clock syncing\n");
            Sys.syncClock();
            return;
        } else {
            //TODO (mchodzikiewicz): add failed wifi conn counter tests
            auto unsuccessfulWifiCounterRead = Registry.getEntry("FailWifiConn");
            if (!unsuccessfulWifiCounterRead) {
                Registry.storeEntry("FailWifiConn", 1);
                Sys.deepSleep(20s);
            }
            auto unsuccessfulWifiCounter = unsuccessfulWifiCounterRead.value().get<int>();
            printf("Wifi conn counter is now %i\n", unsuccessfulWifiCounter);
            auto PrevSuccessfulConnectionRead = Registry.getEntry("PrevSuccConn");
            bool PreviousSuccessfulConnection;
            if(!PrevSuccessfulConnectionRead){
                PreviousSuccessfulConnection = false;
                Registry.storeEntry("PrevSuccessConn",false);
                printf("PreviousSuccessfulConnection info does not exist, setting false\n");
            } else {
                PreviousSuccessfulConnection = PrevSuccessfulConnectionRead.value().get<bool>();
            }
            if (PreviousSuccessfulConnection) {
                if (unsuccessfulWifiCounter > 10) {
                    printf("Conn counter overflowed, going to long sleep!\n");
                    Sys.deepSleep(600s);
                } else {
                    Registry.storeEntry("FailWifiConn", unsuccessfulWifiCounter + 1);
                    Sys.deepSleep(60s);
                }
            } else {
                if (unsuccessfulWifiCounter > 3) {
                    printf("Conn counter overflowed, forgetting WiFi configuration!\n");
                    wifiConfigurator.forgetConfig();
                    Registry.storeEntry("FailWifiConn", 0);
                    Sys.reboot();
                } else {
                    Registry.storeEntry("FailWifiConn", unsuccessfulWifiCounter + 1);
                    Sys.deepSleep(20s);
                }
            }
        }
        Wifi.returnClient(std::move(wifiClient));
    } else {
        printf("WifiConfig not present\n");
        auto WifiServer = Wifi.getServer();
        WifiServer->init();
        WifiServer->setOnStartedServingCallback([]() {
            printf("APP: Started WiFi AP\n");
        });
        WifiServer->setOnClientConnectedCallback([&WifiServer]() {
            printf("Client connected! Waiting for credentials\n");
            WifiServer->setUnusedTimeout(30s);
        });
        WifiServer->setOnClientDisconnectedCallback([this, &wifiConfigurator, &WifiServer]() {
            printf("Client disconnected.\n");
            if (wifiConfigurator.hasConfig()) {
                printf("Credentials provided, attempting WiFi connection!\n");
                WifiServer->stop();
                Sys.deepSleep(1s);
            } else {
                printf("Continuing hosting wifi\n");
                WifiServer->setUnusedTimeout(30s);
            }
        });
        WifiServer->setOnNotInUseCallback([this, &WifiServer]() {

        });
        WifiServer->setUnusedTimeout(30s);
        etl::string<32> SSID;
        SSID.resize(SSID.max_size());
        SSID.resize(snprintf(SSID.data(), SSID.size(), "BeerBob_%s", Sys.getDeviceId().data()));
        WifiServer->setSSID(SSID);
        WifiServer->setPassword("open.brewery");

        wifiConfigurator.setupWizard();

        WifiServer->serve();
        std::this_thread::sleep_for(5s);
        HttpServer.serve("192.168.1.1", 80);
        std::chrono::seconds time_dangling = 0s;
        while (1) {
            std::this_thread::sleep_for(5s);
            time_dangling += 5s;
            printf("Dangling at wifi config\n");
            if (time_dangling >= 1800s) {
                printf("WIFI HOTSPOT UNUSED TIMEOUT\n"
                       "going to deep sleep, press the button to wake me up\n");
                WifiServer->stop();
                Sys.deepSleep();
            }
        }
//        Wifi.returnServer(std::move(WifiServer));
    }
}

void BeerBob::registerIfNeeded(System::Registry &Registry, MeasurementCollector &collector) {
    etl::string_view token;
    auto RegisteredToken = Registry.getEntry("RToken");
    if (!RegisteredToken) {
        printf("No token stored. Asking backend.\n");
        auto receivedToken = collector.registerDevice(HttpReqBuffer, HttpResBuffer, Sys.getDeviceId());
        if (!receivedToken) {
            printf("Token not received\n");
            Sys.deepSleep(5s);
        }
        printf("Received token: %.31s\n", receivedToken.value().data());
        storeState(MeasurementCollector::Calibration());
        //TODO: try to get rid of this excessive string
        etl::string<32> tokenCopy(receivedToken.value().cbegin(), receivedToken.value().cbegin() + 31);
        Registry.storeEntry("RToken", tokenCopy);
        printf("Token received: \"%.31s\"\n", tokenCopy.c_str());
        token = tokenCopy;
    } else {
        token = RegisteredToken.value().get<etl::string<32>>();
        printf("Token restored: \"%.31s\"\n", token.data());
    }
    AuthStringBuffer.resize(AuthStringBuffer.max_size());
    AuthStringBuffer.resize(
            snprintf(AuthStringBuffer.data(), AuthStringBuffer.size(), "Authorization: Bearer %.31s\n", token.data()));
    HttpClient.setAuthorization(AuthStringBuffer);
}

void BeerBob::storeState(MeasurementCollector::Settings settings) {
    State state = State::Incorrect;
    if(settings.has_value()) {
        if (settings.value().is_type<MeasurementCollector::Calibration>()) {
            printf("Storing Calibration state\n");
            state = State::Calibration;
        } else if (settings.value().is_type<MeasurementCollector::MeasurementSync>()) {
            printf("Storing MeasurementSync state, wkup interval: %ims, measurements per packet: %i\n",
                   static_cast<int>(settings.value().get<MeasurementCollector::MeasurementSync>().wakeupInterval.count()),
                   settings.value().get<MeasurementCollector::MeasurementSync>().measurementsPerPacket
            );
            Registry.storeEntry("WkupInterval",
                                settings.value().get<MeasurementCollector::MeasurementSync>().wakeupInterval);
            Registry.storeEntry("MeasPPackets",
                                settings.value().get<MeasurementCollector::MeasurementSync>().measurementsPerPacket);
            state = State::MeasurementSync;
        } else if (settings.value().is_type<MeasurementCollector::WifiAP>()) {
            printf("Storing WifiAP state\n");
            state = State::WifiAP;
        } else if (settings.value().is_type<MeasurementCollector::Refurbish>()) {
            printf("Storing Refurbish state\n");
            state = State::Refurbish;
        }
    }
    if (state != State::Incorrect) {
        Registry.storeEntry("CurrentState", static_cast<int>(state));
    }
}

MeasurementCollector::Settings BeerBob::retrieveState() {
    MeasurementCollector::Settings settings;
    auto wakeupInterval = Registry.getEntry("WkupInterval");
    auto measPerPackets = Registry.getEntry("MeasPPackets");
    std::chrono::milliseconds wkupint = 5000ms;
    int measppack = 6;
    if (wakeupInterval) {
        wkupint = wakeupInterval.value().get<std::chrono::milliseconds>();
    }
    if (measPerPackets) {
        measppack = measPerPackets.value().get<int>();
    }
    auto currState = Registry.getEntry("CurrentState");
    if (currState) {
        switch (static_cast<State>(currState.value().get<int>())) {
            case State::Calibration:
                settings = MeasurementCollector::Calibration();
                break;
            case State::MeasurementSync:
                settings = MeasurementCollector::MeasurementSync{wkupint, measppack};
                break;
            case State::WifiAP:
                settings = MeasurementCollector::WifiAP();
                break;
            case State::Refurbish:
                settings = MeasurementCollector::Refurbish();
                break;
            case State::Incorrect:
                break;
            default:
                break;
        }
    }
    return settings;
}

std::pair<Measurement,Geometry::Point3D<int>> BeerBob::performCalMeasurement() {
    etl::vector<Geometry::Point3D<float>, 100> acc_vec;
    for (int i = 0; i < 100; i++) {
        auto acc = Accelerometer.read().value();
        acc_vec.push_back(static_cast<Geometry::Point3D<float>>(acc));
        Sys.delay(80ms);
    }
    auto avg_acc = Geometry::Point3D<float>::avg(etl::array_view<Geometry::Point3D<float>>{acc_vec});
    auto reoriented_acc = Geometry::Point3D<float>::reorientate(avg_acc,Geometry::Point3D<float>::Origin::Y_negative);
    printf("reoriented_acc = [%3.5f,%3.5f,%3.5f]\n", reoriented_acc.X, reoriented_acc.Y, reoriented_acc.Z);
    Measurement meas;
    auto tilt = Geometry::Point3D<float>::tilt(reoriented_acc);
    meas.cosine = tilt.Pitch;
    if (std::isnan(meas.cosine)) meas.cosine = -1;
    meas.temperature = Thermometer.read().value();
    meas.timestamp = Sys.now();
    return {meas,avg_acc};
}




Measurement BeerBob::performMeasurement(Calibration::Calibrator & calibrator) {
    etl::vector<Geometry::Point3D<float>, 100> acc_vec;
    for (int i = 0; i < 100; i++) {
        auto acc = Accelerometer.read().value();
        acc_vec.push_back(static_cast<Geometry::Point3D<float>>(acc));
        Sys.delay(80ms);
    }
    auto avg_acc = Geometry::Point3D<float>::avg(etl::array_view<Geometry::Point3D<float>>{acc_vec});
    auto reoriented_acc = Geometry::Point3D<float>::reorientate(avg_acc,Geometry::Point3D<float>::Origin::Y_negative);
    printf("reoriented_acc = [%3.5f,%3.5f,%3.5f]\n", reoriented_acc.X, reoriented_acc.Y, reoriented_acc.Z);
    Measurement meas;
    auto tilt = Geometry::Point3D<float>::tilt(reoriented_acc);
    meas.cosine = tilt.Pitch;
    if (std::isnan(meas.cosine)) meas.cosine = -2;
    meas.temperature = Thermometer.read().value();
    meas.timestamp = Sys.now();

    return meas;
}


//
// Created by edwin on 21/03/19.
//

#include <cstdio>
#include "Diagnostics.h"

void Diagnostics::start() {
    using namespace std::literals;
    HttpServer.addGetResource("/diagnostics",[this](etl::istring & resBuffer) -> Communication::HttpResponse {
        char sensor_readout_buffer[200];
        //TODO: move readouts to other object that refreshes vals in different thread and get cached ones here
        etl::string_view ID = Sys.getDeviceId();

        etl::vector<Geometry::Point3D<int>, 10> acc_vec;
        for (int i = 0; i < 10; i++) {
            auto acc = Accelerometer.read().value();
            acc_vec.push_back(acc);
            Sys.delay(80ms);
        }
        auto acc_avg = Geometry::Point3D<int>::avg(etl::array_view<Geometry::Point3D<int>>(acc_vec));
        auto acc_reoriented = Geometry::Point3D<int>::reorientate(acc_avg,Geometry::Point3D<int>::Origin::Y_negative);
        auto tilt = Geometry::Point3D<int>::tilt(acc_reoriented);
        Sensor::Temperature temp = Thermometer.read().value_or(123);
        Sensor::Temperature humi = HumiditySensor.read().value_or(123);
        auto parsed_size = snprintf(sensor_readout_buffer,200,"Device ID: %s<br>"
                                                              "X: %i<br>Y: %i<br>Z:%i<br>"
                                                              "Temperature: %f<br>"
                                                              "Humidity: %f<br>"
                                                              "Battery: %f<br>"
                                                              "Pitch: %2.5f | Roll: %2.5f<br>"
                                                              ,ID.cbegin()
                                                              ,acc_reoriented.X,acc_reoriented.Y,acc_reoriented.Z
                                                              ,(float)temp
                                                              ,(float)humi
                                                              ,Sys.batteryVoltage()
                                                              ,tilt.Pitch
                                                              ,tilt.Roll
                                                              );
        resBuffer.assign(sensor_readout_buffer,parsed_size);
        return Communication::HttpResponse{200,etl::string_view(resBuffer.cbegin(),resBuffer.size())};
    });
}

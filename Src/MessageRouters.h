//
// Created by mchodzikiewicz on 3/8/19.
//

#ifndef BEERBOB_APPLICATION_MESSAGEROUTERS_H
#define BEERBOB_APPLICATION_MESSAGEROUTERS_H

struct MessageRouters {
    enum {
        DontCare = 0,
        BeerBob
    };
};

#endif //BEERBOB_APPLICATION_MESSAGEROUTERS_H

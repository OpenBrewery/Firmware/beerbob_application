//
// Created by mchodzikiewicz on 5/12/19.
//

#ifndef BEERBOB_APPLICATION_SYSTEMREST_H
#define BEERBOB_APPLICATION_SYSTEMREST_H

#include <System.h>
#include <Registry.h>
#include <HttpServer.h>

class SystemRest {
public:
    SystemRest(
            System::System & sys,
            System::Registry & registry,
            Communication::HttpServer & httpServer
    ) : Sys(sys), Registry(registry), HttpServer(httpServer)
    {

    }

    void start();

    System::System & Sys;
    System::Registry & Registry;
    Communication::HttpServer & HttpServer;

};



#endif //BEERBOB_APPLICATION_SYSTEMREST_H

//
// Created by mchodzikiewicz on 3/27/19.
//

#ifndef BEERBOB_APPLICATION_MEASUREMENTCOLLECTOR_H
#define BEERBOB_APPLICATION_MEASUREMENTCOLLECTOR_H

#include <Point3D.h>
#include <etl/array_view.h>
#include <etl/variant.h>
#include "HttpClient.h"
#include "Thermometer.h"
#include "HumiditySensor.h"

using Cosine = float;

struct Measurement {
    Cosine cosine;
    Sensor::Temperature temperature;
    std::chrono::system_clock::time_point timestamp;
};

struct SystemStateInfo {
    std::chrono::milliseconds lastAwakeTime;
    float batteryVoltage;       //TODO: change to Sensor::Voltage and write voltmeter class
    Sensor::Humidity humidity;
    std::chrono::system_clock::time_point timestamp;
    etl::string_view version{"Version not set"};
};



class MeasurementCollector {
public:
    struct MeasurementSync {
        std::chrono::milliseconds wakeupInterval;
        int measurementsPerPacket;
    };
    struct Calibration {};
    struct WaterRefPoint {};
    struct WortRefPoint {
        float WortBlg;
    };
    struct WifiAP {};
    struct Fota {
        etl::string<256> url;
    };
    struct Refurbish {};
    struct Off{};

    using Settings = std::optional<etl::variant<MeasurementSync
                                , Calibration
                                , WifiAP
                                , Fota
                                , Refurbish
                                , Off
                                , WaterRefPoint
                                , WortRefPoint
                                >>;

    explicit MeasurementCollector(Communication::HttpClient & client);

    Settings sendCalibration(etl::istring & reqBuffer, etl::istring & resBuffer, etl::string_view deviceId, SystemStateInfo info, Measurement meas);
    Settings sendMultipleMeasurement(etl::istring & reqBuffer, etl::istring & resBuffer, etl::string_view deviceId, SystemStateInfo info, etl::array_view<Measurement> meas);

    std::optional<etl::string_view> registerDevice(etl::istring & reqBuffer, etl::istring & resBuffer, etl::string_view deviceId);

protected:
    Settings deserializeSettings(etl::string_view response);
    std::optional<etl::string_view> deserializeToken(etl::string_view response);

private:
    Communication::HttpClient & HttpClient;
};


#endif //BEERBOB_APPLICATION_MEASUREMENTCOLLECTOR_H

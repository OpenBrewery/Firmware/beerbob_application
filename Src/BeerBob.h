//
// Created by mchodzikiewicz on 2/11/19.
//

#ifndef PROJECT_BEERBOB_H
#define PROJECT_BEERBOB_H


#include <Thermometer.h>
#include <HumiditySensor.h>
#include <Accelerometer.h>
#include <HttpClient.h>
#include <HttpServer.h>
#include "Wifi.h"
#include "WifiConfigurator.h"
#include <thread>
#include <Registry.h>
#include <System.h>
#include <FirmwareManager.h>
#include "MeasurementCollector.h"

#include "MessageRouters.h"
#include "Calibrator.h"

class BeerBob : public etl::message_router<BeerBob,WifiConfigured,WifiConfigAborted> {
public:

    enum class State {
          Calibration
        , MeasurementSync
        , WifiAP
        , Refurbish
        , Fota
        , Incorrect = 0xFF
    };

    BeerBob(System::System & sys
            , System::Registry & registry
            , System::FirmwareManager & fw_manager
            , Peripheral::Wifi & wifi
            , Sensor::Accelerometer & accelerometer
            , Sensor::Thermometer & thermometer
            , Sensor::HumiditySensor & humiditySensor
            , Communication::HttpClient & httpClient
            , Communication::HttpServer & httpServer
            , etl::istring & httpResBuffer
            , etl::istring & httpReqBuffer
            , etl::istring & authTokenBuffer
    );

    bool run(bool pressed);

    void attemptWifiConnection(WifiConfigurator & wifiConfigurator);
    void registerIfNeeded(System::Registry & Registry, MeasurementCollector & collector);
    void calibrationLoop(System::System & sys, MeasurementCollector & collector, Calibration::Calibrator &calibrator);
    void measurementSynchronization(System::System & sys, MeasurementCollector & collector, Calibration::Calibrator &calibrator);

    void storeState(MeasurementCollector::Settings settings);
    MeasurementCollector::Settings retrieveState();

    void on_receive(etl::imessage_router& sender, const WifiConfigured& msg)  {

    }

    void on_receive(etl::imessage_router& sender, const WifiConfigAborted& msg)  {

    }

    void on_receive_unknown(etl::imessage_router& sender, const etl::imessage& msg){

    }



private:
    Measurement performMeasurement(Calibration::Calibrator & calibrator);
    std::pair<Measurement,Geometry::Point3D<int>> performCalMeasurement();

    System::System & Sys;
    System::Registry & Registry;
    System::FirmwareManager & FwManager;
    Peripheral::Wifi & Wifi;
    Sensor::Accelerometer & Accelerometer;
    Sensor::Thermometer & Thermometer;
    Sensor::HumiditySensor & HumiditySensor;
    Communication::HttpClient & HttpClient;
    Communication::HttpServer & HttpServer;


    etl::istring & AuthStringBuffer;
    etl::istring & HttpResBuffer;
    etl::istring & HttpReqBuffer;
    float batteryV;

};


#endif //PROJECT_BEERBOB_H

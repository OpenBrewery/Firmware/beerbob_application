//
// Created by mchodzikiewicz on 5/12/19.
//

#include <cstdio>
#include "SystemRest.h"

void SystemRest::start() {
    HttpServer.addPostResource("/erase",[this](etl::istring & resBuffer,etl::string_view content){
        Registry.eraseRegistry();
        return Communication::HttpResponse{200,etl::string_view("Erased")};
    });

    HttpServer.addPostResource("/reboot",[this](etl::istring & resBuffer,etl::string_view content){
        Sys.reboot();
        return Communication::HttpResponse{200,etl::string_view("Reboot")};
    });

    HttpServer.addPostResource("/sleep",[this](etl::istring & resBuffer,etl::string_view content){
        Sys.deepSleep();

        return Communication::HttpResponse{200,etl::string_view("Sleep")};
    });

    HttpServer.addPostResource("/sleepForMs",[this](etl::istring & resBuffer,etl::string_view content){
        std::chrono::milliseconds sleepTime(atoi(content.data()));
        Sys.deepSleep(sleepTime);
        resBuffer.resize(resBuffer.max_size());
        resBuffer.resize(snprintf(resBuffer.data(),resBuffer.size(),"Sleep for %ims",sleepTime.count()));
        return Communication::HttpResponse{200,etl::string_view(resBuffer)};
    });

}
